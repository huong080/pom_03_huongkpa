package BankGuru99;

public class HomePageUI {
	public static final String WELCOME_MESSAGE = "//marquee[contains(text(),\"Welcome To Manager's Page of Guru99 Bank\")]";
	public static final String NEW_CUSTOMER_LINK = "//a[contains(.,'New Customer')]";
	public static final String EDIT_CUSTOMER_LINK = "//a[contains(.,'Edit Customer')]";
	public static final String NEW_ACCOUNT_LINK = "//a[contains(.,'New Account')]";
	public static final String EDIT_ACCOUNT_LINK = "//a[contains(.,'Edit Account')]";
	public static final String DEPOSIT_LINK = "//a[contains(.,'Deposit')]";
	public static final String WITHDRAWAL_LINK = "//a[contains(.,'Withdrawal')]";
	public static final String TRANSFER_LINK = "//a[contains(.,'Fund Transfer')]";
	public static final String BALANCE_ENQUIRY_LINK = "//a[contains(.,'Balance Enquiry')]";
	public static final String DELETE_CUSTOMER_LINK = "//a[contains(.,'Delete Customer')]";
	public static final String DELETE_ACCOUNT_LINK = "//a[contains(.,'Delete Account')]";
}
