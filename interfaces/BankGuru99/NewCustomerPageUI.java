package BankGuru99;

public class NewCustomerPageUI {
	public static final String CUSTOMER_NAME_TEXTBOX = "//input[@name='name']";
	public static final String DATE_OF_BIRTH_TEXTBOX = "//input[@name='dob']";
	public static final String ADDRESS_TEXTAREA = "//textarea[@name='addr']";
	public static final String CITY_TEXTBOX = "//input[@name='city']";
	public static final String STATE_TEXTBOX = "//input[@name='state']";
	public static final String PIN_TEXTBOX = "//input[@name='pinno']";
	public static final String MOBILE_NUMBER_TEXTBOX = "//input[@name='telephoneno']";
	public static final String EMAIL_TEXTBOX = "//input[@name='emailid']";
	public static final String PASSWORD_TEXTBOX = "//input[@name='password']";
	public static final String SUBMIT_BUTTON = "//input[@name='sub']";

	public static final String ERROR_MSG_NUMERIC = "//label[contains(.,'Numbers are not allowed')]";
	public static final String ERROR_MSG_SPECIAL_CHARACTER = "//label[contains(.,'Special characters are not allowed')]";
	public static final String ERROR_MSG_BLANK_SPACE_AT_FIRST_CHARACTER = "//label[contains(.,'First character can not have space')]";
	
	public static final String ERROR_MSG_BLANK_CUSTOMER_NAME = "//label[contains(.,'Customer name must not be blank')]";
	public static final String ERROR_MSG_BLANK_DATE_OF_BIRTH = "//label[contains(.,'Date Field must not be blank')]";
	public static final String ERROR_MSG_BLANK_ADDRESS = "//label[contains(.,'Address Field must not be blank')]";
	public static final String ERROR_MSG_BLANK_SPACE_CITY = "//label[contains(.,'City Field must not be blank')]";
	public static final String ERROR_MSG_BLANK_SPACE_STATE = "//label[contains(.,'State must not be blank')]";
	public static final String ERROR_MSG_BLANK_SPACE_PIN = "//label[contains(.,'PIN Code must not be blank')]";
	public static final String ERROR_MSG_BLANK_SPACE_MOBILE_NUMBER = "//label[contains(.,'Mobile no must not be blank')]";
	public static final String ERROR_MSG_BLANK_SPACE_EMAIL = "//label[contains(.,'Email-ID must not be blank')]";
	
	public static final String ERROR_MSG_CHARACTERS_ARE_NOT_ALLOWED_PIN = "//label[contains(.,'Characters are not allowed')]";	
	public static final String ERROR_MSG_MUST_HAVE_6_DIGITS_PIN = "//label[contains(.,'PIN Code must have 6 Digits')]";	
	public static final String ERROR_MSG_BLANK_SPACE_INSDE_VALUE = "//label[contains(.,'Characters are not allowed')]";	
	public static final String ERROR_MSG_INVALID_EMAIL = "//label[contains(.,'Email-ID is not valid')]";	
	public static final String ERROR_MSG_INVALID_EMAIL_SPACE_INSIDE = "//label[@id='message9']";	
}
