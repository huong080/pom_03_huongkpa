package commons;

public class Constants {
	public static final String USERNAME = "mngr75460";
	public static final String PASSWORD = "tYnytym";
	public static final String NUMERIC1 = "1234";
	public static final String NUMERIC2 = "name123";
	public static final String SPECIAL_CHARACTER1 = "!@#";
	public static final String SPECIAL_CHARACTER2 = "name!@#";
	public static final String SPECIAL_CHARACTER3 = "1234!@#";
	public static final String SPECIAL_CHARACTER4 = "886636!@12";
	public static final String SPECIAL_CHARACTER5 = "!@88662682";
	public static final String SPECIAL_CHARACTER6 = "88663682!@";
	public static final String BLANK_SPACE_AT_FIRST_CHARACTER = " ";
	public static final String BLANK_SPACE_INSIDE_VALUE = "123 23 ";
	public static final String NUMBER_AND_CHARACTER = "1234PIN";
	public static final String MORE_THAN_6_DIGITS = "123456789";
	public static final String LESS_THAN_6_DIGITS = "12345";
	public static final String INVALID_EMAIL1 = "guru99@gmail";
	public static final String INVALID_EMAIL2 = "guru99";
	public static final String INVALID_EMAIL3 = "Guru99@";
	public static final String INVALID_EMAIL4 = "guru99@gmail.";
	public static final String INVALID_EMAIL5 = "guru99gmail.com";
	public static final String EMAIL_WITH_SPACE = "guru   99  @ gmail .com";
	
}
