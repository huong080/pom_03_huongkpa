package commons;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AbstractPage {

	public void clickToElement(WebDriver driver, String controlName) {
		WebElement element = driver.findElement(By.xpath(controlName));
		element.click();
	}

	public void typeToElement(WebDriver driver, String controlName, String value) {
		WebElement element = driver.findElement(By.xpath(controlName));
		element.clear();
		element.sendKeys(value);
	}

	public String getTextElement(WebDriver driver, String controlName) {
		WebElement element = driver.findElement(By.xpath(controlName));
		return element.getText();
	}

	public boolean isControlDisplayed(WebDriver driver, String controlName) {
		WebElement element = driver.findElement(By.xpath(controlName));
		System.out.println(element.getText());
		return element.isDisplayed();
	}

	public void pressTab(WebDriver driver, String controlName) {
		WebElement element = driver.findElement(By.xpath(controlName));
		element.sendKeys(Keys.TAB);
	}
}
