package pages;

import org.openqa.selenium.WebDriver;
import BankGuru99.NewCustomerPageUI;
import commons.AbstractPage;

public class NewCustomerPage extends AbstractPage {
	WebDriver driver;

	public NewCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	public NewCustomerPage clickOnCustomerNameTextbox() {
		clickToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtCustomerName() {
		pressTab(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public void typeToCustomerName(String customerName) {
		typeToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TEXTBOX, customerName);
	}

	public void isErrorMessageBlankCustomerNameDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_CUSTOMER_NAME);
	}

	public void isErrorMessageNumericCustomerNameDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_NUMERIC);
	}

	public void isErrorMessageSpecialCharacterDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_SPECIAL_CHARACTER);
	}

	public void isErrorMessageBlankSpaceAtFirstCharacterDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_AT_FIRST_CHARACTER);
	}

	public NewCustomerPage clickOnAddressTextarea() {
		clickToElement(driver, NewCustomerPageUI.ADDRESS_TEXTAREA);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtAddress() {
		pressTab(driver, NewCustomerPageUI.ADDRESS_TEXTAREA);
		return new NewCustomerPage(driver);
	}

	public void typeToAddress(String address) {
		typeToElement(driver, NewCustomerPageUI.ADDRESS_TEXTAREA, address);
	}

	public void isErrorMessageBlankAddressDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_ADDRESS);
	}

	public NewCustomerPage clickOnCityTextbox() {
		clickToElement(driver, NewCustomerPageUI.CITY_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtCity() {
		pressTab(driver, NewCustomerPageUI.CITY_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public void typeToCity(String city) {
		typeToElement(driver, NewCustomerPageUI.CITY_TEXTBOX, city);
	}

	public void isErrorMessageBlankCityDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_CITY);
	}

	public void isErrorMessageNumericCityDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_NUMERIC);
	}

	public NewCustomerPage clickOnStateTextbox() {
		clickToElement(driver, NewCustomerPageUI.STATE_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtState() {
		pressTab(driver, NewCustomerPageUI.STATE_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public void typeToState(String state) {
		typeToElement(driver, NewCustomerPageUI.STATE_TEXTBOX, state);
	}

	public void isErrorMessageBlankStateDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_STATE);
	}

	public void isErrorMessageNumericStateDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_NUMERIC);
	}

	public NewCustomerPage clickOnPINTextbox() {
		clickToElement(driver, NewCustomerPageUI.PIN_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtPIN() {
		pressTab(driver, NewCustomerPageUI.PIN_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public void typeToPIN(String state) {
		typeToElement(driver, NewCustomerPageUI.PIN_TEXTBOX, state);
	}

	public void isErrorMessageBlankPINDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_PIN);
	}

	public void isErrorMessageCharactersAreNotAllowedPINDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_CHARACTERS_ARE_NOT_ALLOWED_PIN);
	}

	public void isErrorMessagePINMustHave6DigitsDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_MUST_HAVE_6_DIGITS_PIN);
	}

	public void isErrorMessageBlankSpaceInsideValueDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_INSDE_VALUE);
	}

	public NewCustomerPage clickOnMobileNumberTextbox() {
		clickToElement(driver, NewCustomerPageUI.MOBILE_NUMBER_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtMobileNumber() {
		pressTab(driver, NewCustomerPageUI.MOBILE_NUMBER_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public void typeToMobileNumber(String state) {
		typeToElement(driver, NewCustomerPageUI.MOBILE_NUMBER_TEXTBOX, state);
	}

	public void isErrorMessageBlankMobileNumberDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_MOBILE_NUMBER);
	}

	public NewCustomerPage clickOnEmailTextbox() {
		clickToElement(driver, NewCustomerPageUI.EMAIL_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public NewCustomerPage pressTabOnKeyboardAtEmail() {
		pressTab(driver, NewCustomerPageUI.EMAIL_TEXTBOX);
		return new NewCustomerPage(driver);
	}

	public void typeToEmail(String state) {
		typeToElement(driver, NewCustomerPageUI.EMAIL_TEXTBOX, state);
	}

	public void isErrorMessageBlankEmailDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_BLANK_SPACE_EMAIL);
	}

	public void isErrorMessageInvalidEmailDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_INVALID_EMAIL);
	}

	public void isErrorMessageEmailWithSpaceDisplayed() {
		isControlDisplayed(driver, NewCustomerPageUI.ERROR_MSG_INVALID_EMAIL_SPACE_INSIDE);
	}
}