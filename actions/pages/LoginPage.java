package pages;

import org.openqa.selenium.WebDriver;
import BankGuru99.LoginPageUI;
import commons.AbstractPage;

public class LoginPage extends AbstractPage {
	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	public void typeToUserName(String userName) {
		typeToElement(driver, LoginPageUI.USERNAME_TEXTBOX, userName);

	}

	public void typeToPassword(String password) {
		typeToElement(driver, LoginPageUI.PASSWORD_TEXTBOX, password);

	}

	public HomePage clickToLoginButton() {
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);
		return new HomePage(driver);
	}

}
