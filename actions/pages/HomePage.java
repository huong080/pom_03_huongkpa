package pages;

import org.openqa.selenium.WebDriver;
import BankGuru99.HomePageUI;
import commons.AbstractPage;

public class HomePage extends AbstractPage {
	WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void isWelcomeMessageDisplayed() {
		isControlDisplayed(driver, HomePageUI.WELCOME_MESSAGE);
	}
	
	public NewCustomerPage clickOnNewCustomerLink() {
		clickToElement(driver, HomePageUI.NEW_CUSTOMER_LINK);
		return new NewCustomerPage(driver);
	}
}
