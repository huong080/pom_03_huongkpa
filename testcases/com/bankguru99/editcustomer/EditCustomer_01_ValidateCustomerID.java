package com.bankguru99.editcustomer;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import commons.AbstractTest;
import pages.EditCustomerPage;

public class EditCustomer_01_ValidateCustomerID extends AbstractTest {
	WebDriver driver;

	@BeforeClass
	public void beforeClass(String browser, String url) {
		driver = openBrowser(browser, url);

		new EditCustomerPage(driver);
	}

	@Test
	public void TC_01_CustomerIDCannoBeEmpty() {
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
