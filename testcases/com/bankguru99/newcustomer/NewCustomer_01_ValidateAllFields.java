package com.bankguru99.newcustomer;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import pages.HomePage;
import pages.LoginPage;
import pages.NewCustomerPage;
import commons.Constants;

public class NewCustomer_01_ValidateAllFields extends AbstractTest {
	WebDriver driver;

	@Parameters({ "browser", "url" })
	@BeforeClass
	public void beforeClass(String browser, String url) {
		driver = openBrowser(browser, url);

		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		newCustomerPage = new NewCustomerPage(driver);
	}

	@Test(enabled = true)
	public void TC_01_NameCannotBeEmpty() throws Exception {
		loginPage.typeToUserName(Constants.USERNAME);
		loginPage.typeToPassword(Constants.PASSWORD);
		homePage = loginPage.clickToLoginButton();
		homePage.isWelcomeMessageDisplayed();
		homePage.clickOnNewCustomerLink();
		newCustomerPage.clickOnCustomerNameTextbox();
		newCustomerPage.pressTabOnKeyboardAtCustomerName();
		newCustomerPage.isErrorMessageBlankCustomerNameDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_02_NameCannotBeNumeric() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToCustomerName(Constants.NUMERIC1);
		newCustomerPage.isErrorMessageNumericCustomerNameDisplayed();
		newCustomerPage.typeToCustomerName(Constants.NUMERIC2);
		newCustomerPage.isErrorMessageNumericCustomerNameDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_03_NameCannotBeSpecialCharacter() throws Exception {
		newCustomerPage.typeToCustomerName(Constants.SPECIAL_CHARACTER1);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		newCustomerPage.typeToCustomerName(Constants.SPECIAL_CHARACTER2);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_04_NameCannotHaveFristCharacterAsBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToCustomerName(Constants.BLANK_SPACE_AT_FIRST_CHARACTER);
		newCustomerPage.isErrorMessageBlankSpaceAtFirstCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_05_AddressCannotBeEmpty() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.clickOnAddressTextarea();
		newCustomerPage.pressTabOnKeyboardAtAddress();
		newCustomerPage.isErrorMessageBlankAddressDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_06_AddressCannotHaveFirstCharacterAsBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToAddress(Constants.BLANK_SPACE_AT_FIRST_CHARACTER);
		newCustomerPage.isErrorMessageBlankSpaceAtFirstCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_07_CityCannotBeEmpty() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.clickOnCityTextbox();
		newCustomerPage.pressTabOnKeyboardAtCity();
		newCustomerPage.isErrorMessageBlankCityDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_08_CityCannotBeNumeric() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToCity(Constants.NUMERIC1);
		newCustomerPage.isErrorMessageNumericCityDisplayed();
		newCustomerPage.typeToCity(Constants.NUMERIC2);
		newCustomerPage.isErrorMessageNumericCityDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_09_CityCannotBeSpecialCharacter() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToCity(Constants.SPECIAL_CHARACTER1);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		newCustomerPage.typeToCity(Constants.SPECIAL_CHARACTER2);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_10_CityCannotHaveFirstCharacterAsBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToCity(Constants.BLANK_SPACE_AT_FIRST_CHARACTER);
		newCustomerPage.isErrorMessageBlankSpaceAtFirstCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_11_StateCannotBeEmpty() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.clickOnStateTextbox();
		newCustomerPage.pressTabOnKeyboardAtState();
		newCustomerPage.isErrorMessageBlankStateDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_12_StateCannotBeNumeric() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToState(Constants.NUMERIC1);
		newCustomerPage.isErrorMessageNumericStateDisplayed();
		newCustomerPage.typeToState(Constants.NUMERIC2);
		newCustomerPage.isErrorMessageNumericStateDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = false)
	public void TC_13_StateCannotBeSpecialCharacter() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToState(Constants.SPECIAL_CHARACTER1);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		newCustomerPage.typeToState(Constants.SPECIAL_CHARACTER2);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_14_StateCannotHaveFirstCharacterAsBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToState(Constants.BLANK_SPACE_AT_FIRST_CHARACTER);
		newCustomerPage.isErrorMessageBlankSpaceAtFirstCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_15_PINIsNotAllowedWithCharacters() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToPIN(Constants.NUMBER_AND_CHARACTER);
		newCustomerPage.isErrorMessageCharactersAreNotAllowedPINDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_16_PINCannotBeEmpty() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.clickOnPINTextbox();
		newCustomerPage.pressTabOnKeyboardAtPIN();
		newCustomerPage.isErrorMessageBlankPINDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_17_PINMustHave6Digits() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToPIN(Constants.MORE_THAN_6_DIGITS);
		newCustomerPage.isErrorMessagePINMustHave6DigitsDisplayed();
		newCustomerPage.typeToPIN(Constants.LESS_THAN_6_DIGITS);
		newCustomerPage.isErrorMessagePINMustHave6DigitsDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_18_PINCannotBeSpecialCharacter() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToPIN(Constants.SPECIAL_CHARACTER1);
		newCustomerPage.pressTabOnKeyboardAtPIN();
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		newCustomerPage.typeToPIN(Constants.SPECIAL_CHARACTER3);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_19_PINCannotHaveFirstCharacterAsBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToPIN(Constants.BLANK_SPACE_AT_FIRST_CHARACTER);
		newCustomerPage.isErrorMessageBlankSpaceAtFirstCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_20_PINCannotHaveBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToPIN(Constants.BLANK_SPACE_INSIDE_VALUE);
		newCustomerPage.isErrorMessageBlankSpaceInsideValueDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_21_MobileNumberCannotBeEmpty() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.clickOnMobileNumberTextbox();
		newCustomerPage.pressTabOnKeyboardAtMobileNumber();
		newCustomerPage.isErrorMessageBlankMobileNumberDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_22_MobileNumberCannotHaveFirstCharacterAsBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToMobileNumber(Constants.BLANK_SPACE_AT_FIRST_CHARACTER);
		newCustomerPage.isErrorMessageBlankSpaceAtFirstCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_23_MobileNumberCannotHaveBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToMobileNumber(Constants.BLANK_SPACE_INSIDE_VALUE);
		newCustomerPage.isErrorMessageBlankSpaceInsideValueDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_24_MobileNumberCannotBeSpecialCharacter() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToMobileNumber(Constants.SPECIAL_CHARACTER4);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		refreshBrowser(driver);
		newCustomerPage.typeToMobileNumber(Constants.SPECIAL_CHARACTER5);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		refreshBrowser(driver);
		newCustomerPage.typeToMobileNumber(Constants.SPECIAL_CHARACTER6);
		newCustomerPage.isErrorMessageSpecialCharacterDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_25_EmailCannotBeEmpty() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.clickOnEmailTextbox();
		newCustomerPage.pressTabOnKeyboardAtEmail();
		newCustomerPage.isErrorMessageBlankEmailDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_26_EmailMustBeInCorrectFormat() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToEmail(Constants.INVALID_EMAIL1);
		newCustomerPage.isErrorMessageInvalidEmailDisplayed();
		refreshBrowser(driver);
		newCustomerPage.typeToEmail(Constants.INVALID_EMAIL2);
		newCustomerPage.isErrorMessageInvalidEmailDisplayed();
		refreshBrowser(driver);
		newCustomerPage.typeToEmail(Constants.INVALID_EMAIL3);
		newCustomerPage.isErrorMessageInvalidEmailDisplayed();
		refreshBrowser(driver);
		newCustomerPage.typeToEmail(Constants.INVALID_EMAIL4);
		newCustomerPage.isErrorMessageInvalidEmailDisplayed();
		refreshBrowser(driver);
		newCustomerPage.typeToEmail(Constants.INVALID_EMAIL5);
		newCustomerPage.isErrorMessageInvalidEmailDisplayed();
		Thread.sleep(500);
	}

	@Test(enabled = true)
	public void TC_27_EmailCannotHaveBlankSpace() throws Exception {
		refreshBrowser(driver);
		newCustomerPage.typeToEmail(Constants.EMAIL_WITH_SPACE);
		newCustomerPage.isErrorMessageEmailWithSpaceDisplayed();
		Thread.sleep(500);
	}

	@AfterClass
	public void afterClass() {
		// driver.quit();
	}

	private HomePage homePage;
	private LoginPage loginPage;
	private NewCustomerPage newCustomerPage;

}
